# Netatmo skill for Snips

## Installation with Sam
The easiest way to use this skill is to install it with [Sam](https://docs.snips.ai/reference/sam):

```
sam install actions -g https://framagit.org/dohseven/snips-skill-netatmo.git
```

## Netatmo credentials
This skill requires an access to the Netatmo API.

* Register on https://dev.netatmo.com
* Create an app on https://dev.netatmo.com/myaccount/createanapp. This app only requires the `read station` scope.
* Retrieve the _Client id_ and the _Client secret_, which will be used by Snips.

## Acknowledgment
This Snips skill is based on the [`lnetatmo`](https://github.com/philippelt/netatmo-api-python/) package. Many thanks to [Philippe Larduinat](https://github.com/philippelt) for creating it!
