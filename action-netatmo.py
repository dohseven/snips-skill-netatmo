#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from hermes_python.hermes import Hermes
from hermes_python.ffi.utils import MqttOptions
from difflib import get_close_matches
import lnetatmo
import configparser

CONFIG_INI = 'config.ini'
MQTT_IP_ADDR = 'localhost'
MQTT_PORT = 1883
MQTT_ADDR = '{}:{}'.format(MQTT_IP_ADDR, str(MQTT_PORT))


class SnipsConfigParser(configparser.ConfigParser):
    def to_dict(self):
        return {
            section: {
                name: option for name, option in self.items(section)
            }
            for section
            in self.sections()
        }


def read_configuration_file(configuration_file):
    """
    Read the configuration file passed as a parameter and
    parse the content into a dict.
    """
    try:
        with open(configuration_file, encoding='utf-8') as f:
            conf_parser = SnipsConfigParser()
            conf_parser.read_file(f)
            return conf_parser.to_dict()
    except (IOError, configparser.Error) as e:
        return dict(e)


def netatmo_authentication(configuration_file):
    """
    Read the configuration file and use the parameters
    to connect to Netatmo API.
    """
    config = read_configuration_file(configuration_file)
    client_id = config.get('secret').get('client_id')
    client_secret = config.get('secret').get('client_secret')
    username = config.get('secret').get('username')
    password = config.get('secret').get('password')

    return lnetatmo.ClientAuth(clientId=client_id,
                               clientSecret=client_secret,
                               username=username,
                               password=password,
                               scope='read_station')


def get_module_name(intent_message):
    """
    Retrieve the module name from the intent.
    """
    if intent_message.slots.module:
        res = intent_message.slots.module[0].slot_value.value.value
        print('Module name: {}'.format(res))
        return res
    return None


def get_netatmo_data(intent_message, characteristic):
    """
    1. Connect to Netatmo API, retrieve the data of the weather station.
    2. Retrieve the module name from the intent and find the closest
       module name corresponding in the weather station data.
    3. Try to retrieve the value for the given module and characteristic.

    Returns a tuple indicating:
    - If the module was found.
    - The module name
    - The characteristic value.
    """
    netatmo_client = netatmo_authentication(CONFIG_INI)
    weather_data = lnetatmo.WeatherStationData(netatmo_client)
    last_data = weather_data.lastData()

    module_name = get_module_name(intent_message)
    if not module_name:
        return (False, None, None)
    modules_list = weather_data.modulesNamesList()
    modules_match = get_close_matches(module_name, modules_list, 1)

    if len(modules_match):
        try:
            value = last_data[modules_match[0]][characteristic]
        except KeyError:
            value = None
        return (True, modules_match[0].lower(), value)

    return (False, module_name, None)


def get_not_found_sentence(module):
    """
    Verbalize that the corresponding module was not found.
    """
    if module:
        sentence = 'Désolé, je n\'ai pas trouvé le module {}'.format(module)
    else:
        sentence = 'Désolé, je n\'ai pas trouvé le module correspondant'

    return sentence


def get_netatmo_temperature(hermes, intent_message):
    """
    Retrieve the temperature measured by the module (if available)
    and verbalize it.
    """
    (found, module, value) = get_netatmo_data(intent_message, 'Temperature')

    if found:
        if value:
            sentence = 'Le module {} mesure une température de {} degrés'\
                       .format(module, str(value))
        else:
            sentence = 'Désolé, le module {} ne mesure pas la température'\
                       .format(module)
    else:
        sentence = get_not_found_sentence(module)

    hermes.publish_end_session(intent_message.session_id, sentence)


def get_netatmo_humidity(hermes, intent_message):
    """
    Retrieve the humidity measured by the module (if available)
    and verbalize it.
    """
    (found, module, value) = get_netatmo_data(intent_message, 'Humidity')

    if found:
        if value:
            sentence = 'Le module {} mesure une humidité de {} pour cent'\
                       .format(module, str(value))
        else:
            sentence = 'Désolé, le module {} ne mesure pas l\'humidité'\
                       .format(module)
    else:
        sentence = get_not_found_sentence(module)

    hermes.publish_end_session(intent_message.session_id, sentence)


def get_netatmo_co2_level(hermes, intent_message):
    """
    Retrieve the CO2 level measured by the module (if available)
    and verbalize it.
    """
    (found, module, value) = get_netatmo_data(intent_message, 'CO2')

    if found:
        if value:
            sentence = 'Le module {} mesure un niveau de c o 2 de {} p p m'\
                       .format(module, str(value))
        else:
            sentence = 'Désolé, le module {} ne mesure pas le niveau de c o 2'\
                       .format(module)
    else:
        sentence = get_not_found_sentence(module)

    hermes.publish_end_session(intent_message.session_id, sentence)


def get_netatmo_air_quality(hermes, intent_message):
    """
    Retrieve the CO2 level measured by the module (if available)
    and verbalize it as an air quality.
    """
    (found, module, value) = get_netatmo_data(intent_message, 'CO2')

    if found:
        if value:
            if value <= 1000:
                air_quality = 'très bonne'
            elif value <= 1500:
                air_quality = 'bonne'
            elif value <= 2000:
                air_quality = 'moyenne'
            else:
                air_quality = 'mauvaise'
            sentence = 'La qualité de l\'air est {} selon le module {}'\
                       .format(air_quality, module)
        else:
            sentence = 'Désolé, le module {} ne mesure pas '\
                       'la qualité de l\'air' .format(module)
    else:
        sentence = get_not_found_sentence(module)

    hermes.publish_end_session(intent_message.session_id, sentence)


def get_netatmo_noise_level(hermes, intent_message):
    """
    Retrieve the noise level measured by the module (if available)
    and verbalize it.
    """
    (found, module, value) = get_netatmo_data(intent_message, 'Noise')

    if found:
        if value:
            sentence = 'Le module {} mesure un niveau de bruit de {} décibels'\
                       .format(module, str(value))
        else:
            sentence = 'Désolé, le module {} ne mesure pas le niveau de bruit'\
                       .format(module)
    else:
        sentence = get_not_found_sentence(module)

    hermes.publish_end_session(intent_message.session_id, sentence)


def get_netatmo_pressure(hermes, intent_message):
    """
    Retrieve the pressure measured by the module (if available)
    and verbalize it.
    """
    (found, module, value) = get_netatmo_data(intent_message, 'Pressure')

    if found:
        if value:
            sentence = 'Le module {} mesure une pression de {} hectopascal'\
                       .format(module, str(value))
        else:
            sentence = 'Désolé, le module {} ne mesure pas la pression'\
                       .format(module)
    else:
        sentence = get_not_found_sentence(module)

    hermes.publish_end_session(intent_message.session_id, sentence)


def get_netatmo_characteristic(hermes, intent_message):
    """
    Check if the confidence score is sufficient to process the intent.
    Dispatch the intent to the corresponding processor if this is the case.
    """
    if intent_message.intent.confidence_score < 0.8:
        print('Ignore intent, confidence is too low ({} < {})'.
              format(intent_message.intent.confidence_score, 0.8))
        hermes.publish_end_session(intent_message.session_id, '')
        return

    intent_name = intent_message.intent.intent_name
    if intent_name == 'dohseven:getNetatmoTemperature':
        get_netatmo_temperature(hermes, intent_message)
    elif intent_name == 'dohseven:getNetatmoHumidity':
        get_netatmo_humidity(hermes, intent_message)
    elif intent_name == 'dohseven:getNetatmoCo2Level':
        get_netatmo_co2_level(hermes, intent_message)
    elif intent_name == 'dohseven:getNetatmoAirQuality':
        get_netatmo_air_quality(hermes, intent_message)
    elif intent_name == 'dohseven:getNetatmoNoiseLevel':
        get_netatmo_noise_level(hermes, intent_message)
    elif intent_name == 'dohseven:getNetatmoPressure':
        get_netatmo_pressure(hermes, intent_message)


if __name__ == '__main__':
    config = read_configuration_file(CONFIG_INI)

    # Check if the configuration file is available
    if not config:
        print('[Error] No configuration file \'{}\' found!'.format(CONFIG_INI))
        exit(1)

    # Check if all necessary parameters are available in the configuration file
    for key in ['client_id', 'client_secret', 'username', 'password']:
        if config.get('secret').get(key) is None \
                or len(config.get('secret').get(key)) == 0:
            print('[Error] You have to set a \'{}\' for this skill to work'
                  .format(key))
            exit(2)

    mqtt_opts = MqttOptions(broker_address=MQTT_ADDR)
    with Hermes(mqtt_options=mqtt_opts) as h:
        h.subscribe_intent('dohseven:getNetatmoTemperature',
                           get_netatmo_characteristic)  \
         .subscribe_intent('dohseven:getNetatmoHumidity',
                           get_netatmo_characteristic)  \
         .subscribe_intent('dohseven:getNetatmoCo2Level',
                           get_netatmo_characteristic)  \
         .subscribe_intent('dohseven:getNetatmoAirQuality',
                           get_netatmo_characteristic)  \
         .subscribe_intent('dohseven:getNetatmoNoiseLevel',
                           get_netatmo_characteristic)  \
         .subscribe_intent('dohseven:getNetatmoPressure',
                           get_netatmo_characteristic)  \
         .loop_forever()
